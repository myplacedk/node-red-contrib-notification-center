[![pipeline status](https://gitlab.com/myplacedk/node-red-contrib-notification-center/badges/master/pipeline.svg)](https://gitlab.com/myplacedk/node-red-contrib-notification-center/commits/firstrelease_)
[![npm @latest](https://img.shields.io/npm/v/node-red-contrib-notification-center.svg)](https://www.npmjs.com/package/node-red-contrib-notification-center)
[![npm downloads](https://img.shields.io/npm/dt/node-red-contrib-notification-center.svg)](https://www.npmjs.com/package/node-red-contrib-notification-center)
[![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)](https://www.apache.org/licenses/LICENSE-2.0)

node-red-contrib-notification-center
====================================

<a href="http://nodered.org" target="_new">Node-RED</a> notification hub.

A single place to send all notifications through, so they can all be handled in a consistent way.

![Example flow](https://gitlab.com/myplacedk/node-red-contrib-notification-center/raw/master/images/screenshot.png)

Install
-------

Run the following command in your Node-RED user directory - typically `~/.node-red`

        npm install node-red-contrib-notification-center

### Examples

#### Example 1: Complete flow

You can get the complete flow from the illustration by opening Node Red, open the hamburger menu (top right corner), 
choose Import -> Examples -> notification center -> Complete flow.

#### Example 2: Parsing data into notifications

Here `msg.payload` contains various status information about my heater. This `function` transforms the most important 
data into notifications according to my preferences.

    const pelletsLow = msg.payload.hopperDaysLeft <= 2 || msg.payload.hopperContentKg < 20;
    
    msg.payload = [
        {
            "category": "stoker",
            "key": "alarm",
            "severity": "high",
            "text": msg.payload.alert ? msg.payload.stateText : null
        },
        {
            "category": "stoker",
            "key": "hopperWarning",
            "severity": "medium",
            "text": pelletsLow ? "About " + msg.payload.hopperDaysLeft + " days of pellets left" : null
        },
        {
            "category": "stoker",
            "key": "state",
            "severity": "low",
            "text": msg.payload.stateText
        }
    ]
    
    return msg;


const NC = require('./NC');

module.exports = function(RED) {
    function NotificationCenterNode(config) {
        RED.nodes.createNode(this, config);
        const node = this;

        node.on('input', msg => {
            const db = node.context().get('db') || [];
            const nc = new NC(db);

            try {
                const result = nc.parse(msg.payload);
                if (result !== null) {
                    msg.payload = result.changed;
                    node.send([null, msg]);
                    msg.payload = result.all;
                    node.send([msg, null]);
                }
                node.context().set('db', nc.db);

                const all = nc.getAll();
                node.status({
                    fill: 'blue',
                    shape: 'dot',
                    text: all.high.length + '-' + all.medium.length + '-' + all.low.length,
                });
            } catch (e) {
                node.warn(e.message);
                node.warn(e);
                node.status({fill: 'red', shape: 'ring', text: e.message});
            }
        });
    }

    RED.nodes.registerType('notification-center', NotificationCenterNode);
};

module.exports = class NC {
    constructor(db) {
        this.db = db;
    }

    parse(payload) {
        this.changed = {high: [], medium: [], low: []};

        if (payload.notifications || payload.high || payload.medium || payload.low) {
            if (typeof payload.clearCategory === 'string') {
                this.markCategoryForDeletion(payload.clearCategory);
            }

            payload.high && this.parseCollection(payload.high);
            payload.medium && this.parseCollection(payload.medium);
            payload.low && this.parseCollection(payload.low);
            payload.notifications && this.parseCollection(payload.notifications);

            if (typeof payload.clearCategory === 'string') {
                this.deleteMarked();
            }
        } else {
            this.parseCollection(payload);
        }

        if (payload.silent
            || this.changed.high.length + this.changed.medium.length + this.changed.low.length === 0) {
            return null;
        } else {
            return {all: this.getAll(), changed: this.changed};
        }
    }

    parseCollection(collection) {
        if ((typeof collection) === 'string') {
            try {
                collection = JSON.parse(collection);
            } catch (e) {
                throw new Error(
                    'Expected object, array or JSON string, got JSON parse error "'
                    + e.message + '" in "' + collection + '"');
            }

        }

        if (Array.isArray(collection)) {
            for (let value of collection) {
                this.parseNotification(value);
            }
        } else {
            if (typeof collection === 'object' || typeof collection === 'string') {
                this.parseNotification(collection);
            } else {
                throw new Error('Expected object, array or JSON string, got ' + (typeof collection));
            }
        }
    }

    parseNotification(notification) {
        notification = NC.validate(notification);

        const cleannotification = {
            severity: notification.severity || 'low',
            category: String(notification.category),
            key: String(notification.key),
            text: notification.text ? String(notification.text) : null,
        };
        if (notification.hasOwnProperty('more')) {
            cleannotification.more = notification.more;
        }
        if (notification.hasOwnProperty('url')) {
            cleannotification.url = notification.url;
        }
        if (notification.hasOwnProperty('data')) {
            cleannotification.data = notification.data;
        }

        const index = this.getIndex(cleannotification);

        if (index >= 0 && NC.isEquivalent(cleannotification, this.db[index])) {
            this.db[index] = cleannotification;
            return;
        }

        if (cleannotification.text === null) {
            if (index >= 0) {
                this.deleteByIndex(index, notification);
            }
        } else {
            if (index < 0) {
                this.db.push(cleannotification);
            } else {
                this.db[index] = cleannotification;
            }
            this.changed[cleannotification.severity].push(cleannotification);
        }

    }

    deleteByIndex(index) {
        const notification = this.db[index];
        notification.text = null;
        delete notification.markedForDeletion;
        this.changed[notification.severity].push(notification);
        this.db.splice(index, 1); // Remove element from array
    }

    markCategoryForDeletion(category) {
        for (let i = 0; i < this.db.length; i++) {
            if (this.db[i].category === category) {
                this.db[i].markedForDeletion = true;
            }
        }
    }

    deleteMarked(){
        for (let i = this.db.length - 1; i >= 0; i--) {
            if (this.db[i].markedForDeletion) {
                this.deleteByIndex(i);
            }
        }
    }

    static isEquivalent(a, b) {
        if (a.severity !== b.severity
            || a.category !== b.category
            || a.key !== b.key
            || a.text !== b.text
            || a.more !== b.more
            || a.url !== b.url) {
            return false;
        }
        if (a.hasOwnProperty('data') || b.hasOwnProperty('data')) {
            return JSON.stringify(a.data) === JSON.stringify(b.data);
        }
        return true;
    }

    static validate(notification) {
        if ((typeof notification) === 'string') {
            try {
                notification = JSON.parse(notification);
            } catch (e) {
                throw new Error('Expected object or JSON string, got JSON parse error "'
                    + e.message + '" in "' + notification + '"');
            }

            if ((typeof notification) !== 'object') {
                throw new Error('Expected object or JSON string, got JSON ' + (typeof notification));
            }
        } else {
            if ((typeof notification) !== 'object') {
                throw new Error('Expected object or JSON string, got ' + (typeof notification));
            }
        }

        if (!notification.category) {
            throw new Error('Missing category');
        }

        if (!notification.key) {
            throw new Error('Missing key');
        }

        if (notification.severity) {
            if (!(notification.severity === 'high'
                || notification.severity === 'medium'
                || notification.severity === 'low')) {
                throw new Error('Invalid severity, expected high, medium or low, got ' + notification.severity);
            }
        }
        return notification;
    }

    getIndex(notification) {
        for (let i = 0; i < this.db.length; i++) {
            if (this.db[i].category === notification.category
                && this.db[i].key === notification.key) {
                return i;
            }
        }
        return -1;
    }

    getAll() {
        const all = {high: [], medium: [], low: []};
        for (let n of this.db) {
            all[n.severity].push(n);
        }
        return all;
    }
};

const chai = require('chai');
const assert = chai.assert;

const NC = require('../main/NC');
const fixtures = require('./fixtures/notifications.json');

describe('notification-center', function() {

    it('should receive single notification', function() {
        const nc = new NC([]);
        const result = nc.parse(fixtures.high1);

        assert.deepEqual(result.all, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1]);
    });

    it('should receive array', function() {
        const nc = new NC([]);
        const result = nc.parse([fixtures.high1]);

        assert.deepEqual(result.all, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1]);
    });

    it('should receive JSON', function() {
        const nc = new NC([]);
        const result = nc.parse(JSON.stringify(fixtures.high1));

        assert.deepEqual(result.all, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1]);
    });

    it('should receive JSON array', function() {
        const nc = new NC([]);
        const result = nc.parse(JSON.stringify([fixtures.high1]));

        assert.deepEqual(result.all, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1]);
    });

    it('should receive nested JSON array', function() {
        const nc = new NC([]);
        const result = nc.parse([JSON.stringify(fixtures.high1)]);

        assert.deepEqual(result.all, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1]);
    });

    it('should receive two notifications', function() {
        const nc = new NC([]);
        nc.parse(fixtures.high1);
        const result = nc.parse(fixtures.high2);

        assert.deepEqual(result.all, {
            high: [fixtures.high1, fixtures.high2],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high2],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1, fixtures.high2]);
    });

    it('should override notification', function() {
        const nc = new NC([]);
        nc.parse(fixtures.high1);
        let result = nc.parse(fixtures.high1b);

        assert.deepEqual(result.all, {
            high: [fixtures.high1b],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1b],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1b]);

        result = nc.parse(fixtures.high1b_plus_url);

        assert.deepEqual(result.all, {
            high: [fixtures.high1b_plus_url],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1b_plus_url],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high1b_plus_url]);
    });

    it('should delete notification', function() {
        const nc = new NC([]);
        nc.parse(fixtures.high1);
        nc.parse(fixtures.high2);
        const result = nc.parse(fixtures.high1d);

        assert.deepEqual(result.all, {
            high: [fixtures.high2],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1d],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high2]);
    });

    it('should delete category', function() {
        const nc = new NC([]);
        nc.parse(fixtures.high1);
        let result = nc.parse({clearCategory: 'abc', notifications: []});

        assert.isNotNull(result);

        assert.deepEqual(result.all, {
            high: [],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high1d],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, []);

        nc.parse(fixtures.high1);
        result = nc.parse({clearCategory: 'abc', notifications: [fixtures.high2]});

        assert.isNotNull(result);

        assert.deepEqual(result.all, {
            high: [fixtures.high2],
            medium: [],
            low: [],
        });

        assert.deepEqual(result.changed, {
            high: [fixtures.high2, fixtures.high1d],
            medium: [],
            low: [],
        });

        assert.deepEqual(nc.db, [fixtures.high2]);
    });

    it('should change empty text to null', function() {
        const nc = new NC([]);
        nc.parse(fixtures.high1);
        const result = nc.parse(fixtures.empty);

        assert.isNull(result.changed.high[0].text);

    });

    it('should throw error on invalid input', function() {
        const nc = new NC([]);

        function trythis(notif, errormessage) {
            try {
                nc.parse(notif);
                assert.fail('Should throw error');
            } catch (e) {
                assert.propertyVal(e, 'message', errormessage);
            }
        }

        trythis(true, 'Expected object, array or JSON string, got boolean');
        trythis('heh', 'Expected object, array or JSON string, got JSON parse error ' +
            '"Unexpected token h in JSON at position 0" in "heh"');
        trythis(['heh'], 'Expected object or JSON string, got JSON parse error ' +
            '"Unexpected token h in JSON at position 0" in "heh"');
        trythis({}, 'Missing category');
        trythis({category: 'x'}, 'Missing key');
        trythis(JSON.stringify({category: 'x'}), 'Missing key');
    });

    it('should not repeat itself', function() {
        const nc = new NC([]);

        nc.parse(fixtures.high1);
        let result = nc.parse(fixtures.high1);
        assert.isNull(result);

        nc.parse(fixtures.high1d);
        result = nc.parse(fixtures.high1d);
        assert.isNull(result);
    });

    it('should be silent when asked', function() {
        const nc = new NC([]);

        nc.parse({high: fixtures.high1});
        let result = nc.parse({high: fixtures.high1b, silent: true});
        assert.isNull(result);

        assert.deepEqual(nc.db, [fixtures.high1b]);

    });

});

const chai = require('chai');
const assert = chai.assert;
const sinon = require('sinon');
const helper = require('node-red-node-test-helper');

const notificationCenter = require('../main/notification-center');
const fixtures = require('./fixtures/notifications');

describe('notification-center Node', function() {

    let flow;

    beforeEach(function() {
        flow = [
            {id: 'nc', type: 'notification-center', name: 'test name', wires: [['all'], ['changed']]},
            {id: 'all', type: 'helper'},
            {id: 'changed', type: 'helper'}];
    });

    afterEach(function() {
        helper.unload();
    });

    it('should be loaded', function(done) {
        helper.load(notificationCenter, flow, function() {
            assert.propertyVal(helper.getNode('nc'), 'name', 'test name');
            done();
        });
    });

    it('should receive', function(done) {
        helper.load(notificationCenter, flow, function() {
            const nc = helper.getNode('nc');
            const all = helper.getNode('all');

            nc.receive({payload: fixtures.high1});
            nc.receive({payload: fixtures.high2});

            all.on('input', function(msg) {
                try {
                    assert.deepEqual(msg.payload, {
                        high: [fixtures.high1b, fixtures.high2],
                        medium: [],
                        low: [],
                    });
                    done();
                } catch (e) {
                    done(e);
                }
            });

            nc.receive({payload: fixtures.high1b});
        });
    });

    it('should not change content that isn\'t payload', function(done) {
        helper.load(notificationCenter, flow, function() {
            const nc = helper.getNode('nc');
            const all = helper.getNode('all');

            all.on('input', function(msg) {
                try {
                    assert.deepEqual(msg.payload, {
                        high: [fixtures.high1],
                        medium: [],
                        low: [],
                    });
                    assert.propertyVal(msg, 'topic', 'foo');
                    assert.propertyVal(msg, 'custom', 'bar');
                    done();
                } catch (e) {
                    done(e);
                }
            });

            nc.receive({payload: fixtures.high1, topic: 'foo', custom: 'bar'});

        });
    });

    it('should send changes', function(done) {
        helper.load(notificationCenter, flow, function() {
            const nc = helper.getNode('nc');
            const changed = helper.getNode('changed');

            nc.receive({payload: fixtures.high1});
            nc.receive({payload: fixtures.high2});

            changed.on('input', function(msg) {
                try {
                    assert.deepEqual(msg.payload, {
                        high: [fixtures.high1b],
                        medium: [],
                        low: [],
                    });
                    done();
                } catch (e) {
                    done(e);
                }
            });

            nc.receive({payload: fixtures.high1b});

        });
    });

    it('should handle errors', function(done) {
        helper.load(notificationCenter, flow, function() {
            const nc = helper.getNode('nc');

            sinon.spy(nc, 'status');

            setTimeout(() => {
                assert.deepEqual(nc.status.getCall(0).args, [
                    {
                        fill: 'red',
                        shape: 'ring',
                        text: 'Missing category',
                    },
                ]);
                done();
            }, 100);

            nc.receive({payload: {}});
        });
    });

    it('should handle silence', function(done) {
        helper.load(notificationCenter, flow, function() {
            const nc = helper.getNode('nc');
            const changed = helper.getNode('changed');

            nc.receive({payload: fixtures.high1});
            nc.receive({payload: fixtures.high2});

            changed.on('input', function(msg) {
                console.log(msg);
                done(new Error('Don\'t send silent changes'));
            });

            nc.receive({payload: {high: fixtures.high1b, silent: true}});

            setTimeout(done, 100);
        });
    });
});
